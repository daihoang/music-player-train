import axios from 'axios';
import { CollectionTrack } from './../type/index';
const getListTrack = async (): Promise<CollectionTrack | Error> => {
  try {
    const result: { data: { collection: CollectionTrack } } = await axios.get(
      'https://api-v2.soundcloud.com/users/713805112/tracks?representation=&client_id=xbqSuLaogVOwYEiYkRmiZvxLLDetbZRD&limit=20&offset=0&linked_partitioning=1&app_version=1667398191&app_locale=en'
    );
    return result.data.collection;
  } catch (error) {
    throw new Error((error as Error).message);
  }
};

const SoundCloudApi = {
  getListTrack,
};
export default SoundCloudApi;
