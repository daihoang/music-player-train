import { DataUsersMockup } from '../mockup';
import { User } from './../type/index';

export const Login = (data: User): User | Error => {
  const user = DataUsersMockup.find((item) => item.email === data.email);
  if (user) {
    if (user.password === data.password) {
      return user;
    } else {
      return new Error('Password wrong');
    }
  }
  return new Error("User doesn't exist");
};
