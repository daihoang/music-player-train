import { SoundCloudState, TrackState } from '../redux/reducer/sound-cloud';

export interface User {
  email: string;
  password: string;
}

export interface Track {
  full_duration: number;
  id: number;
  artwork_url: string;
  title: string;
  user: {
    avatar_url: string;
    username: string;
  };
  position: number;
}
export interface WidgetType {
  play: () => void;
  toggle: () => void;
  next: () => void;
  unbind: (name: string) => void;
  getDuration: () => number;
  getCurrentSound: (callback: (currentSound: number) => void) => number;
  getPosition: (callback: (position: number) => void) => number;
  getVolume: (callback: (volume: number) => void) => void;
  bind: (eventName: string, listener: () => void) => void;
  setVolume: (volume: number) => void;
  seekTo: (data: number) => void;
}

declare global {
  interface Window {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    SC: any;
  }
}

export interface RootStateReducer {
  soundCloud: SoundCloudState;
  track: TrackState;
}
export type CollectionTrack = Track[];
