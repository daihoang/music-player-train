import { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootStateReducer } from '../../type';

import BackIcon from '../../assets/detail/arrow-left.png';
import ThreeDots from '../../assets/detail/dots.png';
import PreviousIcon from '../../assets/detail/previous.png';
import NextIcon from '../../assets/detail/next.png';
import PauseIcon from '../../assets/detail/pause.png';
import PlayIcon from '../../assets/detail/play.png';
import styles from './DetailScreen.module.scss';
import { toggleTrack } from '../../redux/action/sound-cloud';
import useTrackControl from '../../hook/useTrackControl';
import useScrollInfinity from '../../hook/useScrollInfinity';
import InputRange from '../../components/InputRange';

const DetailTrackScreen = () => {
  const dispatch = useDispatch();
  const { onShowDetail, onNextTrack, onPreviousTrack } = useTrackControl();
  const [onInfinityScroll] = useScrollInfinity();
  const { widgetControl, track, isPlaying, loading } = useSelector(
    (state: RootStateReducer) => state.track
  );
  const imgRef = useRef<HTMLImageElement | null>(null);
  const barRef = useRef<HTMLInputElement | null>(null);
  useEffect(() => {
    if (loading && barRef.current) {
      barRef.current.value = '0';
    }
    if (imgRef.current && !loading) {
      onInfinityScroll(imgRef.current);
    }
  }, [loading, track]);

  const handleShowDetail = () => {
    onShowDetail();
  };
  const handleToggleTrack = () => {
    widgetControl?.toggle();
    dispatch(toggleTrack(!isPlaying));
  };
  if (widgetControl) {
    return (
      <section className={styles.container}>
        <div className={styles['header-detail']}>
          <button
            onClick={() => handleShowDetail()}
            className={styles['btn-header']}
          >
            <img src={BackIcon} alt="back home" />
          </button>
          <span className={styles['text-header']}>Now Playing</span>
          <button className={styles['btn-header']}>
            <img src={ThreeDots} alt="more option" />
          </button>
        </div>
        <div className={styles['box-img']}>
          {!loading ? (
            <img
              ref={imgRef}
              className={styles['img-track']}
              src={track.artwork_url}
              alt={track.title + ' art work'}
            />
          ) : (
            'Loading.....'
          )}
        </div>
        <div className={styles['box-info-track']}>
          <h3 className={styles['title-track']}>{track.title}</h3>
          <h4 className={styles['track-author']}>{track.user.username}</h4>
        </div>
        <div className={styles['bar-control']}>
          <InputRange min={0} max={track.full_duration} />
        </div>
        <div className={styles['box-btn-ctrl']}>
          <button
            onClick={() => onPreviousTrack()}
            className={styles['btn-ctrl']}
          >
            <img src={PreviousIcon} alt="previous track" />
          </button>
          <button
            onClick={() => handleToggleTrack()}
            className={styles['btn-toggle']}
          >
            <img src={isPlaying ? PauseIcon : PlayIcon} alt="pause track" />
          </button>
          <button onClick={() => onNextTrack()} className={styles['btn-ctrl']}>
            <img src={NextIcon} alt="next track" />
          </button>
        </div>
      </section>
    );
  } else {
    return <div>Loading.....</div>;
  }
};

export default DetailTrackScreen;
