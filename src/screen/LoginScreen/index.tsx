import React, { useRef } from 'react';
import styles from './LoginPage.module.scss';
import ImgLoginScreen from '../../assets/login-screen/img.png';
import { checkEmailValid, checkPasswordValid } from '../../utils';
import { Login } from '../../apis/Auth';
import { User } from '../../type';

interface LoginScreenProps {
  onSetStateLogin: (state: boolean) => void;
}
const LoginScreen = ({ onSetStateLogin }: LoginScreenProps) => {
  const emailRef = useRef<HTMLInputElement | null>(null);
  const passwordRef = useRef<HTMLInputElement | null>(null);
  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    // onSetStateLogin(true);
    event.preventDefault();
    const email = checkEmailValid(emailRef.current?.value || '');
    const password = checkPasswordValid(passwordRef.current?.value || '');
    if (email instanceof Error) {
      return alert(email.message);
    }
    if (password instanceof Error) {
      return alert(password.message);
    }
    const data: User = {
      email,
      password,
    };
    const result = Login(data);
    if (result instanceof Error) {
      return alert(result.message);
    }
    onSetStateLogin(true);
  };
  return (
    <section className={styles.container}>
      <div className={styles['box-content']}>
        <span className={styles['text-welcome']}>Welcome</span>
        <img
          className={styles['login-img']}
          src={ImgLoginScreen}
          alt="Login Screen Img"
        />
        <form className={styles['form-login']} onSubmit={handleSubmit}>
          <input
            ref={emailRef}
            type="email"
            required
            className={styles['form-input']}
            placeholder="input your email"
          />
          <input
            ref={passwordRef}
            type="password"
            required
            className={styles['form-input']}
            placeholder="input your password"
          />
          <button className={styles['form-btn']}>Login</button>
        </form>
      </div>
    </section>
  );
};

export default LoginScreen;
