import React, { useEffect, useRef } from 'react';
import { RootStateReducer, Track } from '../../type';
import styles from './Listscreen.module.scss';
import { useSelector } from 'react-redux';
import useScrollInfinity from '../../hook/useScrollInfinity';
import useTrackControl from '../../hook/useTrackControl';
interface TrackInfoProps {
  currentTrack: Track | null;
  defaultTrack: Track;
}

const TrackInfo = ({ currentTrack }: TrackInfoProps) => {
  const { loading } = useSelector((state: RootStateReducer) => state.track);
  const list = useSelector((state: RootStateReducer) => state.soundCloud.list);
  const [onInfinityScroll] = useScrollInfinity();
  const { onShowDetail } = useTrackControl();
  const imgRef = useRef<HTMLImageElement | null>(null);
  useEffect(() => {
    if (imgRef.current && !loading) {
      onInfinityScroll(imgRef.current);
    }
  }, [loading]);

  return (
    <div className={styles['box-info']}>
      <h3 className={styles['author-name']}>
        {(currentTrack && currentTrack.user.username) || list[0].user.username}
      </h3>
      <h4 className={styles['track-name']}>
        {(currentTrack && currentTrack.title) || list[0].title}
      </h4>
      <div className={styles['box-img']}>
        {!loading ? (
          <img
            onClick={() => onShowDetail()}
            ref={imgRef}
            className={styles['img-content']}
            src={
              (currentTrack && currentTrack.artwork_url) || list[0].artwork_url
            }
            alt={
              (currentTrack && currentTrack.title) ||
              list[0].title + ' art work'
            }
          />
        ) : (
          'Loading.....'
        )}
      </div>
    </div>
  );
};

export default React.memo(TrackInfo);
