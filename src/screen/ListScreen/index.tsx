import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import ItemTrack from '../../components/ItemTrack';
import { getListTrackRequest } from '../../redux/action/sound-cloud';
import { RootStateReducer } from '../../type/index';
import styles from './Listscreen.module.scss';
import TrackInfo from './TrackInfo';
const ListTrack = () => {
  const dispatch = useDispatch();
  const { track: currentTrack } = useSelector(
    (state: RootStateReducer) => state.track
  );

  const { list } = useSelector((state: RootStateReducer) => state.soundCloud);
  const { loading, error } = useSelector(
    (state: RootStateReducer) => state.soundCloud
  );

  useEffect(() => {
    dispatch(getListTrackRequest());
  }, []);

  if (loading) {
    return (
      <div className={styles.container}>
        <div className={styles['container-loading']}>
          <span>Loading....</span>
        </div>
      </div>
    );
  }
  if (error) {
    return (
      <div className={styles.container}>
        <div className={styles['container-loading']}>
          <span>Xảy ra lỗi khi tải dữ liệu</span>
        </div>
      </div>
    );
  }

  return (
    <div className={styles.container}>
      <TrackInfo currentTrack={currentTrack} defaultTrack={list[0]} />
      <div className={styles['box-track']}>
        <span className={styles['header-box-track']}>My Playlist</span>
        <ul className={styles['track-list']}>
          {list.map((item, index) => {
            return (
              <ItemTrack
                index={index}
                key={item.id}
                track={item}
                isCurrent={currentTrack?.id === item.id}
              />
            );
          })}
        </ul>
      </div>
    </div>
  );
};

export default ListTrack;
