import React, { useEffect, useRef } from 'react';
import { RootStateReducer } from '../../type';
import { useDispatch, useSelector } from 'react-redux';
import { getTrackSuccess } from '../../redux/action/sound-cloud';

const PlayUnder = () => {
  const dispatch = useDispatch();
  const iframeRef = useRef<HTMLIFrameElement | null>(null);
  const { track } = useSelector((state: RootStateReducer) => state.track);
  useEffect(() => {
    const widgetIframe = iframeRef.current;
    const SC = window.SC;
    const widget = SC.Widget(widgetIframe);

    widget.bind(SC.Widget.Events.READY, function () {
      dispatch(getTrackSuccess(widget));
      widget.play();
    });
  }, [track.id]);

  return (
    <div>
      <iframe
        ref={iframeRef}
        title={track.title}
        allow="autoplay"
        src={`https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/${track.id}&amp;`}
      ></iframe>
    </div>
  );
};

export default PlayUnder;
