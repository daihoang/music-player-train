import React from 'react';
import styles from './navbar.module.scss';
import HomeIcon from '../../assets/nav/home.png';
import ShufflerIcon from '../../assets/nav/shuffler.png';
import SearchIcon from '../../assets/nav/search.png';
import UserIcon from '../../assets/nav/user.png';
import { useDispatch, useSelector } from 'react-redux';
import { toggleShuffle } from '../../redux/action/sound-cloud';
import { RootStateReducer } from '../../type';
const Navbar = () => {
  const dispatch = useDispatch();
  const isShuffle = useSelector(
    (state: RootStateReducer) => state.track.isShuffle
  );
  const handleToggleShuffle = () => {
    dispatch(toggleShuffle());
  };
  return (
    <section className={styles.container}>
      <div className={styles['box-nav']}>
        <button className={styles['btn-nav']}>
          <img src={HomeIcon} alt="button move home screen" />
        </button>
        <button
          onClick={() => handleToggleShuffle()}
          className={`${styles['btn-nav']} ${
            isShuffle && styles['btn-active']
          }`}
        >
          <img src={ShufflerIcon} alt="button shuffler list track" />
        </button>
        <button className={styles['btn-nav']}>
          <img src={SearchIcon} alt="button search track" />
        </button>
        <button className={styles['btn-nav']}>
          <img src={UserIcon} alt="Button move profile screen" />
        </button>
      </div>
    </section>
  );
};

export default Navbar;
