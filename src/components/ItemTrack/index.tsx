import React, { useEffect } from 'react';
import IconPlay from '../../assets/play-icon.png';
import IconHeart from '../../assets/heart-icon.png';
import PauseIcon from '../../assets/pause.png';
import styles from './item-track.module.scss';
import PlayUnder from '../PlayerUnder';
import { RootStateReducer, Track } from '../../type';
import { useSelector } from 'react-redux';
import useTrackControl from '../../hook/useTrackControl';
interface TrackProps {
  track: Track;
  index: number;
  isCurrent: boolean;
}

const ItemTrack = ({ track, index, isCurrent }: TrackProps) => {
  const { onNextTrack, onRequestTrack, onToggleTrack, onStoreHistory } =
    useTrackControl();
  const {
    widgetControl,
    isPlaying,
    isShuffle,
    track: currentTrack,
  } = useSelector((state: RootStateReducer) => state.track);
  const handleToggleTrack = () => {
    const currentState = isPlaying;
    if (isCurrent) {
      onToggleTrack(!currentState);
    }
  };
  const handlePlayTrack = (indexTrack: number) => {
    if (isCurrent && widgetControl) {
      handleToggleTrack();
      return;
    }
    handleToggleTrack();
    onStoreHistory();
    onRequestTrack(indexTrack);
  };

  useEffect(() => {
    if (widgetControl && currentTrack.position === index) {
      const SC = window.SC;
      widgetControl.unbind(SC.Widget.Events.FINISH);
      widgetControl.bind(SC.Widget.Events.FINISH, function () {
        onNextTrack();
      });
    }
  }, [isShuffle, widgetControl]);

  return (
    <li className={styles['container-item']}>
      <button
        onClick={() => handlePlayTrack(index)}
        className={`${styles['item-btn']} ${styles['item-btn-play']}`}
      >
        <img
          src={isPlaying && isCurrent ? PauseIcon : IconPlay}
          alt={'icon play music' + track.title}
        />
      </button>
      <span className={styles['item-title']}>{track.title}</span>
      <button className={`${styles['item-btn']} ${styles['item-btn-heart']}`}>
        <img src={IconHeart} alt={'like' + track.title} />
      </button>
      {isCurrent && <PlayUnder />}
    </li>
  );
};

export default React.memo(ItemTrack);
