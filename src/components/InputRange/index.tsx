import { useEffect, useRef } from 'react';
import { useSelector } from 'react-redux';
import useTrackControl from '../../hook/useTrackControl';
import { RootStateReducer } from '../../type';
import styles from './range.module.scss';
interface InputProps {
  min: number;
  max: number;
}

const InputRange = ({ min, max }: InputProps) => {
  const btnRef = useRef<HTMLElement | null>(null);
  const inputRef = useRef<HTMLInputElement | null>(null);
  const progressRef = useRef<HTMLElement | null>(null);
  const { widgetControl, isShowDetail } = useSelector(
    (state: RootStateReducer) => state.track
  );
  const { onSeekTrack } = useTrackControl();
  useEffect(() => {
    if (inputRef.current && progressRef.current && btnRef.current) {
      inputRef.current.value = '0';
      progressRef.current.style.width = 0 + '%';
      btnRef.current.style.left = 0 + '%';
    }
  }, [widgetControl]);
  const changeRange = () => {
    if (inputRef.current && btnRef.current && progressRef.current) {
      const currentValue = parseInt(inputRef.current?.value);
      btnRef.current.style.left = (currentValue / max) * 100 + '%';
      progressRef.current.style.width = (currentValue / max) * 100 + '%';
      inputRef.current.value = currentValue.toString();
      onSeekTrack(currentValue);
    }
  };
  useEffect(() => {
    if (widgetControl) {
      const SC = window.SC;
      widgetControl.bind(SC.Widget.Events.PLAY_PROGRESS, function () {
        widgetControl.getPosition(function (position: number) {
          if (progressRef.current && inputRef.current && btnRef.current) {
            progressRef.current.style.width = (position / max) * 100 + '%';
            btnRef.current.style.left = (position / max) * 100 + '%';
            inputRef.current.value = position.toString();
          }
        });
      });
    }
  }, [widgetControl, isShowDetail]);
  return (
    <div className={styles.container}>
      <input
        ref={inputRef}
        onChange={changeRange}
        className={styles['input-custom']}
        type={'range'}
        min={min}
        max={max}
      />
      <section ref={btnRef} className={styles.selector}>
        <div className={styles['btn-selected']}></div>
      </section>
      <section ref={progressRef} className={styles['propress-bar']}></section>
    </div>
  );
};
export default InputRange;
