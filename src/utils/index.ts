export const checkEmailValid = (email: string): string | Error => {
  const expression =
    /^(?=.{1,254}$)(?=.{1,64}@)[-!#$%&'*+/0-9=?A-Z^_`a-z{|}~]+(\.[-!#$%&'*+/0-9=?A-Z^_`a-z{|}~]+)*@[A-Za-z0-9]([A-Za-z0-9-]{0,61}[A-Za-z0-9])?(\.[A-Za-z0-9]([A-Za-z0-9-]{0,61}[A-Za-z0-9])?)*$/;
  if (expression.test(email)) {
    return email;
  }
  return new Error('Invalid Email');
};

export const checkPasswordValid = (password: string): string | Error => {
  const regexLowerCase = /(.*[a-z].*)/;
  const regexUpperCase = /(.*[A-Z].*)/;
  const regexDigit = /(.*\d.*)/;
  const regexSymbol = /(.*[~!@#$%^&*()_+].*)/;
  if (
    regexLowerCase.test(password) &&
    regexUpperCase.test(password) &&
    regexDigit.test(password) &&
    regexSymbol.test(password) &&
    password.length > 12
  ) {
    return password;
  }
  return new Error(
    'Invalid Password - Password include lowercase, uppercase, number, length > 12, special character (~!@#$%^&*()_+)'
  );
};

export const getRandomWithOneExclusion = (
  lengthOfArray: number,
  indexToExclude: number
) => {
  let rand = -1; //an integer
  while (rand < 0 || rand === indexToExclude) {
    rand = Math.round(Math.random() * (lengthOfArray - 1));
  }

  return rand;
};
