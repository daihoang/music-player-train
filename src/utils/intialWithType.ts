import { Track, WidgetType } from '../type';

export const initialTrack: Track = {
  id: 0,
  artwork_url: '',
  full_duration: 0,
  title: '',
  user: { username: '', avatar_url: '' },
  position: 0,
};

export const initialWidget: WidgetType = {
  play: () => {},
  toggle: () => {},
  next: () => {},
  unbind: () => {},
  getDuration: () => 0,
  getPosition: () => 0,
  getCurrentSound: () => 0,
  getVolume: () => {},
  bind: () => {},
  setVolume: () => {},
  seekTo: () => 0,
};
