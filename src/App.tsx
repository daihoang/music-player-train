import { useState } from 'react';
import { useSelector } from 'react-redux';
import Navbar from './components/Navbar';
import DetailTrackScreen from './screen/DetailTrackScreen';
import ListTrack from './screen/ListScreen';
import LoginScreen from './screen/LoginScreen';
import { RootStateReducer } from './type';

function App() {
  const [isLogin, setIsLogin] = useState<boolean>(false);
  const checkIsLogin = (stateLogin: boolean) => {
    setIsLogin(stateLogin);
  };
  const { isShowDetail } = useSelector(
    (state: RootStateReducer) => state.track
  );
  return (
    <div className="container">
      {!isLogin ? (
        <LoginScreen onSetStateLogin={checkIsLogin} />
      ) : (
        <>
          <ListTrack />
          {isShowDetail && <DetailTrackScreen />}
          <Navbar />
        </>
      )}
    </div>
  );
}

export default App;
