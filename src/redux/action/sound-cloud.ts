import { CollectionTrack, Track, WidgetType } from './../../type/index';
import SoundCloudConstants from '../constants/sound-cloud';
export const getListTrackRequest = () => ({
  type: SoundCloudConstants.GET_LIST_TRACK_REQUEST,
});
export const getListTrackSuccess = (data: CollectionTrack) => ({
  type: SoundCloudConstants.GET_LIST_TRACK_SUCCESS,
  payload: { list: data },
});
export const getListTrackFail = (message: string) => ({
  type: SoundCloudConstants.GET_LIST_TRACK_FAIL,
  payload: message,
});

export const getTrackRequest = (track: Track) => {
  return {
    type: SoundCloudConstants.GET_TRACK_REQUEST,
    payload: { track: track },
  };
};

export const getTrackSuccess = (widget: WidgetType) => ({
  type: SoundCloudConstants.GET_TRACK_SUCCESS,
  payload: { widgetControl: widget },
});

export const getTrackFail = (message: string) => ({
  type: SoundCloudConstants.GET_TRACK_SUCCESS,
  payload: message,
});

export const toggleTrack = (state: boolean) => ({
  type: SoundCloudConstants.TOGGLE_TRACK,
  payload: { isPlaying: state },
});

export const toggleShuffle = () => ({
  type: SoundCloudConstants.TOGGLE_SHUFFLE,
});

export const toggleShowDetail = () => ({
  type: SoundCloudConstants.SHOW_DETAIL,
});

export const storePreviousTrack = () => ({
  type: SoundCloudConstants.STORE_HISTORY_TRACK,
});

export const previousTrack = (newListPrevious: number[]) => {
  return {
    type: SoundCloudConstants.PREVIOUS_TRACK,
    payload: { listPrevious: newListPrevious },
  };
};
