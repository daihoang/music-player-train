import { toggleTrack } from './../action/sound-cloud';
import { CollectionTrack } from './../../type/index';
import { getListTrackSuccess, getListTrackFail } from '../action/sound-cloud';
import SoundCloudApi from '../../apis/SoundCloud';
import { put, call, takeEvery } from 'redux-saga/effects';
import SoundCloudConstants from '../constants/sound-cloud';

function* getListTrack() {
  try {
    const response: CollectionTrack = yield call(SoundCloudApi.getListTrack);
    yield put(getListTrackSuccess(response));
  } catch (error) {
    console.log(error);

    yield put(getListTrackFail('Lỗi khi tải'));
  }
}
function* watchListTrack() {
  yield takeEvery(SoundCloudConstants.GET_LIST_TRACK_REQUEST, getListTrack);
}

function* toggleTrackSaga() {
  yield put(toggleTrack(true));
}

function* watchGetTrack() {
  yield takeEvery(SoundCloudConstants.GET_TRACK_SUCCESS, toggleTrackSaga);
}

const SoundCloudSaga = [watchListTrack(), watchGetTrack()];

export default SoundCloudSaga;
