import { all } from 'redux-saga/effects';
import SoundCloudSaga from './sound-cloud';
export default function* rootSaga() {
  yield all([...SoundCloudSaga]);
}
