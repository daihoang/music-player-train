import { soundCloudReducer, trackReducer } from './sound-cloud';
import { combineReducers } from 'redux';
export default combineReducers({
  soundCloud: soundCloudReducer,
  track: trackReducer,
});
