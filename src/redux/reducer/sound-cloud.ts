import { CollectionTrack, Track, WidgetType } from './../../type/index';
import SoundCloudConstants from '../constants/sound-cloud';
import { initialTrack } from '../../utils/intialWithType';
// state
export interface SoundCloudState {
  list: CollectionTrack;
  error: boolean;
  loading: boolean;
}
export interface TrackState {
  loading: boolean;
  isPlaying: boolean;
  error: boolean;
  next: number | boolean;
  previous: number | boolean;
  isShuffle: boolean;
  track: Track;
  widgetControl: WidgetType | null;
  isShowDetail: boolean;
  listPrevious: number[];
}

// action
export interface TrackAction {
  type: string;
  payload: TrackState;
}
export interface SoundCloudAction {
  type: string;
  payload: SoundCloudState;
}

const initialState: SoundCloudState = {
  list: [initialTrack],
  error: false,
  loading: false,
};
const initialTrackState: TrackState = {
  error: false,
  loading: false,
  isPlaying: false,
  next: false,
  previous: false,
  isShuffle: false,
  isShowDetail: false,
  track: initialTrack,
  widgetControl: null,
  listPrevious: [],
};

export const soundCloudReducer = (
  state: SoundCloudState = initialState,
  action: SoundCloudAction = {
    type: '',
    payload: initialState,
  }
) => {
  switch (action.type) {
    case SoundCloudConstants.GET_LIST_TRACK_REQUEST:
      return { ...state, loading: true };
    case SoundCloudConstants.GET_LIST_TRACK_SUCCESS:
      return { ...state, loading: false, list: action.payload.list };
    case SoundCloudConstants.GET_LIST_TRACK_FAIL:
      return { ...state, loading: false, error: true };
    default:
      return state;
  }
};

export const trackReducer = (
  state: TrackState = initialTrackState,
  action: TrackAction = { type: '', payload: initialTrackState }
) => {
  switch (action.type) {
    case SoundCloudConstants.GET_TRACK_REQUEST:
      return {
        ...state,
        loading: true,
        next: false,
        track: action.payload.track,
      };
    case SoundCloudConstants.GET_TRACK_SUCCESS:
      return {
        ...state,
        loading: false,
        widgetControl: action.payload.widgetControl,
      };
    case SoundCloudConstants.NEXT_TRACK:
      return { ...state, next: action.payload.next };
    case SoundCloudConstants.TOGGLE_TRACK:
      return { ...state, isPlaying: action.payload.isPlaying };
    case SoundCloudConstants.TOGGLE_SHUFFLE:
      return { ...state, isShuffle: !state.isShuffle };
    case SoundCloudConstants.SHOW_DETAIL:
      return { ...state, isShowDetail: !state.isShowDetail };
    case SoundCloudConstants.STORE_HISTORY_TRACK: {
      const currentHistory = state.listPrevious;
      const currentPositionTrack = state.listPrevious.indexOf(
        state.track.position
      );
      if (currentPositionTrack > -1) {
        currentHistory.splice(currentPositionTrack, 1);
      }
      console.log(currentPositionTrack);

      currentHistory.unshift(state.track.position);
      return {
        ...state,
        listPrevious: currentHistory,
      };
    }
    case SoundCloudConstants.PREVIOUS_TRACK: {
      return { ...state, listPrevious: action.payload.listPrevious };
    }
    default:
      return state;
  }
};
