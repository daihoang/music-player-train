// import React from 'react';
// import { render, screen } from '@testing-library/react';
// import App from './App';

import { checkEmailValid, checkPasswordValid } from './utils';

// test('renders learn react link', () => {
//   render(<App />);
//   const linkElement = screen.getByText(/learn react/i);
//   expect(linkElement).toBeInTheDocument();
// });

describe('Testing password', () => {
  const pass = [
    '12345',
    '123456789aa@@',
    '123456789AA@@',
    '123456789aAAA',
    'aaaaaaaaaAA@@',
  ];
  pass.forEach((item, index) => {
    it(`Case ${index + 1} ${item}`, () => {
      const result = checkPasswordValid(item);
      console.log(result);
      expect(result).not.toEqual(item);
    });
  });
});

describe('Test Valid Email', () => {
  const email = ['daihoang.asis', '@ac', '@as.com'];
  email.forEach((item, index) => {
    it(`Case ${index + 1} ${item}`, () => {
      const result = checkEmailValid(item);
      console.log(result);
      expect(result).not.toEqual(item);
    });
  });
});
