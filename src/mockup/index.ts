import { User } from './../type/index';

export const DataUsersMockup: User[] = [
  {
    email: 'admin@gmail.com',
    password: '123456789aA@@',
  },
  {
    email: 'admin2@gmail.com',
    password: '123456789aA@@',
  },
  {
    email: 'admin3@gmail.com',
    password: '123456789aA@@',
  },
  {
    email: 'admin4@gmail.com',
    password: '123456789aA@@',
  },
];
