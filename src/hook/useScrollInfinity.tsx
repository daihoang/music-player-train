import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { RootStateReducer } from '../type';

const useScrollInfinity = () => {
  const { isPlaying } = useSelector((state: RootStateReducer) => state.track);
  const [stateRef, setStateRef] = useState<HTMLImageElement | null>(null);
  const [animationState, setAnimationState] = useState<Animation | null>(null);
  useEffect(() => {
    if (stateRef) {
      const animationImg: Animation = stateRef.animate(
        [
          {
            transform: 'rotate(360deg)',
          },
        ],
        {
          duration: 10000,
          iterations: Infinity,
        }
      );
      setAnimationState(animationImg);
      if (!isPlaying) {
        animationImg.pause();
      }
    }
  }, [stateRef]);
  useEffect(() => {
    if (isPlaying) {
      animationState?.play();
    } else {
      animationState?.pause();
    }
  }, [isPlaying]);
  const setImgRef = (ref: HTMLImageElement | null) => {
    if (ref) {
      setStateRef(ref);
    }
  };
  return [setImgRef];
};

export default useScrollInfinity;
