import { useDispatch, useSelector } from 'react-redux';
import {
  getTrackRequest,
  toggleTrack,
  storePreviousTrack,
  previousTrack,
  toggleShowDetail,
} from '../redux/action/sound-cloud';
import { RootStateReducer } from '../type';
import { getRandomWithOneExclusion } from '../utils';

const useTrackControl = () => {
  const { isShuffle, track, widgetControl, listPrevious } = useSelector(
    (state: RootStateReducer) => state.track
  );
  const list = useSelector((state: RootStateReducer) => state.soundCloud.list);
  const dispatch = useDispatch();
  const onRequestTrack = (index: number) => {
    if (index >= 0 && index <= list.length - 1) {
      const data = list[index];
      data.position = index;
      dispatch(getTrackRequest(data));
    }
  };
  const onToggleTrack = (state: boolean) => {
    if (widgetControl) {
      dispatch(toggleTrack(state));
      widgetControl.toggle();
    }
  };
  const onShowDetail = () => {
    if (widgetControl) {
      dispatch(toggleShowDetail());
    }
  };
  const onStoreHistory = () => {
    if (track.user.username) {
      dispatch(storePreviousTrack());
    }
  };
  const onNextTrack = () => {
    if (track.position + 1 > list.length - 1) {
      return;
    }
    onToggleTrack(false);
    onStoreHistory();
    if (isShuffle) {
      onRequestTrack(getRandomWithOneExclusion(list.length, track.position));
      return;
    }
    onRequestTrack(track.position + 1);
  };
  const onPreviousTrack = () => {
    if (listPrevious.length > 0) {
      const currentList = listPrevious;
      const preTrack = listPrevious[0];
      currentList.shift();
      dispatch(getTrackRequest({ ...list[preTrack], position: preTrack }));
      dispatch(previousTrack(currentList));
    }
  };
  const onSeekTrack = (millisecond: number) => {
    widgetControl?.seekTo(millisecond);
  };
  return {
    onShowDetail,
    onNextTrack,
    onPreviousTrack,
    onRequestTrack,
    onToggleTrack,
    onStoreHistory,
    onSeekTrack,
  };
};

export default useTrackControl;
